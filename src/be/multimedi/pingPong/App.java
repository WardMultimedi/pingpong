package be.multimedi.pingPong;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.VolatileImage;
import java.util.Random;

public class App {
   Canvas canvas;
   KeyboardInput keyboard = new KeyboardInput();
   private static final int CANVAS_WIDTH = 800;
   private static final int CANVAS_HEIGHT = 640;
   private static final int SCREEN_WIDHT = CANVAS_WIDTH / 2;
   private static final int SCREEN_HEIGHT = CANVAS_HEIGHT / 2;

   public static void main(String[] args) {
      App app = new App();
      app.startGame();
   }

   void startGame() {
      Frame frame = new Frame();

      canvas = new Canvas();
      canvas.setPreferredSize(new Dimension(CANVAS_WIDTH, CANVAS_HEIGHT));

      frame.add(canvas);
      frame.pack();

      frame.addWindowListener(new WindowAdapter() {
         @Override
         public void windowClosing(WindowEvent e) {
            super.windowClosing(e);
            System.exit(0);
         }
      });

      frame.addKeyListener(keyboard);
      canvas.addKeyListener(keyboard);
      frame.setResizable(false);
      frame.setVisible(true);

      Thread t = new Thread() {
         boolean isRunning = true;
         VolatileImage img;
         Ball ball;
         Paddle player;
         Paddle ai;

         int totalFrame = 0;
         int fps = 0;
         boolean showFps = false;

         @Override
         public void run() {
            GraphicsConfiguration gc = canvas.getGraphicsConfiguration();
            img = gc.createCompatibleVolatileImage(SCREEN_WIDHT, SCREEN_HEIGHT);

            createGameObjects();

            long startTime = 0;
            long prevTime;

            long lastFPSCheck = 0;


            while (isRunning) {
               prevTime = startTime;
               startTime = System.nanoTime();
               if (prevTime > 0) {
                  float timeDelta = (float) ((startTime - prevTime) / 1_000_000_000D);
                  update(timeDelta);
               }

               if (lastFPSCheck + 1_000_000_000 < startTime) {
                  fps = totalFrame;
                  totalFrame = 0;
                  lastFPSCheck = startTime;
               }

               draw();

               long temp = System.nanoTime() - startTime;
               temp = 1_000_000_000 / 200 - temp;
               if (temp > 0) {
                  try {
                     Thread.sleep(temp / 1_000_000);
                  } catch (InterruptedException e) {
                     //e.printStackTrace();
                  }
               }

            }
         }

         void createGameObjects() {
            Random rnd = new Random();
            ball = new Ball(SCREEN_WIDHT / 2, SCREEN_HEIGHT / 2, 100, rnd.nextInt(101) - 50);

            player = new Paddle(20, SCREEN_HEIGHT / 2);
            ai = new Paddle(SCREEN_WIDHT - 20, SCREEN_HEIGHT / 2);
         }

         void update(float timeDelta) {
            if (keyboard.isKeyPressed(KeyEvent.VK_F1)) {
               showFps = !showFps;
            }

            if (keyboard.isKeyPressed(KeyEvent.VK_DOWN)) {
               player.vY = 60;
            } else if (keyboard.isKeyPressed(KeyEvent.VK_UP)) {
               player.vY = -60;
            }
            player.update(timeDelta);
            player.vY = 0;
            ai.update(timeDelta);

            ball.update(timeDelta);
            //# ball screen edge bouncing
            if (ball.posX + 8 >= SCREEN_WIDHT) {
               ball.vX = -ball.vX;
               ball.posXf -= ((ball.posXf + 8) - SCREEN_WIDHT) * 2;
               ball.posX = (int) ball.posXf;
            } else if (ball.posX < 0) {
               ball.vX = -ball.vX;
               ball.posXf += -ball.posXf * 2;
               ball.posX = (int) ball.posXf;
            }
            if (ball.posY + 8 >= SCREEN_HEIGHT) {
               ball.vY = -ball.vY;
               ball.posYf -= ((ball.posYf + 8) - SCREEN_HEIGHT) * 2;
               ball.posY = (int) ball.posYf;
            } else if (ball.posY < 0) {
               ball.vY = -ball.vY;
               ball.posYf += -ball.posYf * 2;
               ball.posY = (int) ball.posYf;
            }
         }

         void draw() {
            Graphics g = img.getGraphics();
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, SCREEN_WIDHT, SCREEN_HEIGHT);

            player.draw(g);
            ai.draw(g);

            ball.draw(g);

            totalFrame++;
            if (showFps)
               g.drawString("fps: " + fps, 10, 10);

            g = canvas.getGraphics();
            g.drawImage(img, 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT, null);
         }
      };
      t.start();
   }
}
