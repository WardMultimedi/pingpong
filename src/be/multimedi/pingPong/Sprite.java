package be.multimedi.pingPong;

import java.awt.*;

public abstract class Sprite {
   int posX, posY;

   public abstract void draw(Graphics g);

   public abstract void update(float timeDelta);
}
