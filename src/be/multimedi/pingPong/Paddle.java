package be.multimedi.pingPong;

import java.awt.*;

public class Paddle extends Sprite {
   static final int width = 8;
   static final int height = 40;

   float posXf;
   float posYf;
   float vX = 0;
   float vY = 0;

   public Paddle(float posXf, float posYf) {
      this.posXf = posXf - (width/2);
      this.posX = (int) this.posXf;
      this.posYf = posYf - (height/2);
      this.posY = (int)this.posYf;
   }

   @Override
   public void draw(Graphics g) {
      g.setColor(Color.GREEN);
      g.fillRect( posX, posY, width, height);
   }

   @Override
   public void update(float timeDelta) {
      posYf += vY*timeDelta;
      posY = (int)posYf;
   }
}
